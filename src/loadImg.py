# -*- coding: utf-8 -*-

# @name: loadImg
# @author: Ableson
# @date: 2021/8/10 22:39
# @update: 2021/8/10 22:39
# @description: 抓取图片

import requests
import os
from lxml import etree


class DownLoaderImg:

    def __init__(self, url='https://wall.alphacoders.com/search.php?search=One+Piece'):
        self.target = url
        self.header = {
         'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) \
         AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36'
        }

    def find_down(self):

        # noinspection PyBroadException
        try:
            # 创建请求日志文件夹
            if 'log' not in os.listdir('.'):
                os.mkdir(r".\log")

            response = requests.get(self.target, headers=self.header)
            response.encoding = response.apparent_encoding
            info = etree.HTML(response.text)
            img_list = info.xpath('//*[@class="boxgrid"]')
            img_urls = []
            for i in img_list:
                img_urls.append(i.xpath("./a/picture//img/@src")[0])

            return img_urls

        except Exception as e:
            print("未知错误 %s\n" % e)
            with open(r".\log\req_error.txt", 'a', encoding='utf-8') as f:
                f.write("未知错误 %s\n" % e)
            f.close()
