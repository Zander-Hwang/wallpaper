# -*- coding: utf-8 -*-

# @name: 创建挂件窗口
# @author: Ableson
# @date: 2021/8/12 19:35
# @update: 2021/8/12 19:35
# @description: 创建挂件窗口
from src.disposeImg import DisposePendant
from PyQt5.QtWidgets import QWidget, QSystemTrayIcon, QMenu, QLabel, QMessageBox
from PyQt5.QtCore import Qt, QTimer
from PyQt5.QtGui import QPixmap, QCursor, QIcon
import os
import threading
import random


class CreatPendant(QWidget):
    def __init__(self, app):
        super().__init__()
        self.top = 1450
        self.left = 400
        self.width = 250
        self.height = 250
        self.img_num = 1
        self.dispose_img, self.height = DisposePendant().dispose_file(self.width)
        self.img_count = len(os.listdir('./log/{}'.format(self.dispose_img)))
        self.lab = QLabel(self)
        self.label = QLabel(self)
        self.talk_condition = False
        self.set_img(self.img_num)
        self.timer = QTimer()
        self.timer.timeout.connect(self.img_update)
        self.timer.start(100)
        # 是否跟随鼠标
        self.is_follow_mouse = False
        # 宠物拖拽时避免鼠标直接跳到左上角
        self.mouse_drag_pos = self.pos()
        self.init_ui(app)
        # systemTray 系统托盘
        # self.make_sys_tray(app)

    # 设置QT窗口
    def init_ui(self, app):
        desktop_size = app.desktop()
        self.left = desktop_size.width() - self.width
        self.top = desktop_size.height() - self.height - 40
        self.setGeometry(self.left - 200, self.top, self.width + 200, self.height)
        self.setWindowTitle("桌面精灵")
        self.setWindowFlags(Qt.FramelessWindowHint | Qt.WindowStaysOnTopHint | Qt.SubWindow)
        self.setAutoFillBackground(False)
        self.setAttribute(Qt.WA_TranslucentBackground, True)
        self.show()
        self.speaking()

    # 设置图片地址
    def set_img(self, num):
        img_path = './log/{file}/{img}.png'.format(file=self.dispose_img, img=str(num))
        qpi_x_map = QPixmap(img_path)
        self.lab.setGeometry(200, 0, self.width, self.height)
        self.lab.setPixmap(qpi_x_map)

    # 更新显示图片
    def img_update(self):
        if self.img_num < self.img_count:
            self.img_num += 1
        else:
            self.img_num = 1
        self.set_img(self.img_num)

    # 显示说话信息
    def speaking(self):
        def show_label():
            if not self.talk_condition:
                self.label.move(0, 0)
                self.label.show()
                self.label.setMinimumSize(200, 100)
                self.label.setMaximumSize(200, 300)
                self.label.setScaledContents(True)
                self.label.setText("这是测试数据1这是测试数据2这是测试数据3这是测试数据4这是测试数据5这是测试数据6")
                self.label.setStyleSheet("font: bold;"
                                         "font:24px '华文行楷';"
                                         "color: #000;"
                                         "padding: 10px;"
                                         "border: 5px solid #937F71;"
                                         "border-radius: 50%;"
                                         "background: #ffffff")
                self.label.setWordWrap(True)
                self.label.adjustSize()
                threading.Timer(1, hide_label).start()

        def hide_label():
            self.label.setText("")
            self.label.hide()
            self.label.adjustSize()
        timer = QTimer(self)
        timer.timeout.connect(show_label)
        timer.start(random.randint(15000, 35000))

    # 鼠标左键按下时, 宠物将和鼠标位置绑定
    def mousePressEvent(self, event):
        if event.button() == Qt.LeftButton:
            self.is_follow_mouse = True
            self.mouse_drag_pos = event.globalPos() - self.pos()
            event.accept()
            self.setCursor(QCursor(Qt.ClosedHandCursor))

    # 鼠标移动, 则宠物也移动
    def mouseMoveEvent(self, event):
        if Qt.LeftButton and self.is_follow_mouse:
            self.move(event.globalPos() - self.mouse_drag_pos)
            event.accept()

    # 鼠标释放时, 取消绑定
    def mouseReleaseEvent(self, event):
        self.is_follow_mouse = False
        self.setCursor(QCursor(Qt.ArrowCursor))

    # systemTray
    def make_sys_tray(self, app):
        # 系统托盘动作执行
        def act(reason):
            # 鼠标点击icon传递的信号会带有一个整形的值，1是表示单击右键，2是双击，3是单击左键，4是用鼠标中键点击
            if reason == 2 or reason == 3:
                show_tray()

        # 关闭事件
        def close_tray():
            app.setQuitOnLastWindowClosed(False)
            msg = QMessageBox()
            msg.setWindowFlags(Qt.Drawer)
            msg.setIcon(QMessageBox.Question)
            msg.setWindowTitle("提示")
            msg.setText("是否关闭桌面宠物")
            msg.setStandardButtons(QMessageBox.Yes | QMessageBox.No)
            yss = msg.button(QMessageBox.Yes)
            yss.setText('是(Y)')
            no = msg.button(QMessageBox.No)
            no.setText('否(N)')
            msg.exec_()
            if msg.clickedButton() == yss:
                tray.deleteLater()
                app.quit()

        # 隐藏事件
        def hide_tray():
            self.hide()
            self.timer.stop()

        # 显示事件
        def show_tray():
            self.show()
            self.timer.start()

        # 创建托盘
        tray = QSystemTrayIcon(self)
        # 设置托盘图标
        icon = QIcon('./static/favicon.ico')
        tray.setIcon(icon)
        # 托盘图标提示
        tray.setToolTip("桌面精灵")
        # 创建托盘菜单
        menu = QMenu(self)
        menu.addAction('显示', show_tray)
        menu.addAction('隐藏', hide_tray)
        menu.addAction('关闭', close_tray)

        # 将菜单添加到托盘里
        tray.setContextMenu(menu)
        # 系统托盘图标被点击事件
        tray.activated.connect(act)
        tray.show()
