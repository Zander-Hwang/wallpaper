# -*- coding: utf-8 -*-

# @name: loadImg
# @author: Ableson
# @date: 2021/8/10 22:39
# @update: 2021/8/10 22:39
# @description: gif图片处理
from PIL import Image  # 导入PIL的Image包
import os


class DisposePendant:
    def __init__(self):
        self.fileName = './static/img/pendant.gif'

    def dispose_file(self, re_width):
        im = Image.open(self.fileName)  # 使用Image的open函数打开test.gif图像
        (width, height) = im.size
        re_height = int(height * re_width / width)
        file_name = os.path.basename(self.fileName)[:-4]
        # 创建存放每一帧图片的文件夹
        if file_name in os.listdir('./log'):
            if len(os.listdir('./log/{}'.format(file_name))) > 0:
                return file_name, re_height
        os.mkdir(r"./log/%s" % file_name)
        try:
            while True:  # 死循环
                current = im.tell()  # 用tell函数保存当前帧图片，赋值给current
                im.save('./log/%s/' % file_name + str(current + 1) + '.png')  # 调用save函数保存该帧图片
                img = Image.open('./log/%s/' % file_name + str(current + 1) + '.png')
                new_img = img.resize((re_width, re_height), Image.ANTIALIAS)
                new_img.save('./log/%s/' % file_name + str(current + 1) + '.png')
                im.seek(current + 1)  # 调用seek函数获取下一帧图片，参数变为current帧图片+1
                # 这里再次进入循环，当为最后一帧图片时，seek会抛出异常，代码执行except
        except EOFError:
            pass  # 最后一帧时，seek抛出异常，进入这里，pass跳过

        return file_name, re_height
